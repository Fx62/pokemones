import { createApp } from 'vue'
import App from './App.vue'
import "bootstrap/dist/js/bootstrap"
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import "datatables.net-bs4/css/dataTables.bootstrap4.min.css"
import "datatables.net-bs4"

createApp(App).mount('#app')
